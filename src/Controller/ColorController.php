<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\ColorRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Color;
use App\Form\ColorType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * @Route("/color", name="color")
 */

class ColorController extends AbstractController
{
    /**
     * @var JMSSerializerInterface
     */

    private $serializer;



    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function getColor(ColorRepository $repo)
    {
        $color = $repo->findAll();

        $json = $this->serializer->serialize($color, 'json');

        return new JsonResponse($json, 200, [], true);
    }
    /**
     * @Route(methods="POST")
     */
    public function addColor(Request $request, ObjectManager $manager)
    {

        $color = new Color();
        $form = $this->createForm(ColorType::class, $color);

        $form->submit(json_decode($request->getContent(), true));

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($color);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($color, 'json'), 200, [], true);
        }
        return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route("/update/{id}", methods="PATCH")
     */
    public function UpdateColor(Color $color, Request $request, ObjectManager $manager)
    {
        $form = $this->createForm(ColorType::class, $color);

        $form->submit(json_decode($request->getContent(), true), false);

        if($form->isSubmitted() && $form->isValid() ){
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($color, 'json'), 200, [], true);
        }
        return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route("/remove/{id}", methods="DELETE")
     */
     public function RemoveColor(Color $color, ObjectManager $manager){

         $manager->remove($color);
         $manager->flush();

         return new JsonResponse($this->serializer->serialize($color, 'json'), 200, [], true);

     }
}
